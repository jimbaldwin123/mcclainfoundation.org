<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <meta name="google-site-verification" content="bJqrJhpaQn2ODPwtAzuXJNlQslPkRq95u2SzDBZBwWw" />
    <link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <title>McClain Foundation</title><!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"><!-- Custom CSS -->
    <link href="css/bootstrap-social.css" rel="stylesheet"><!-- Social Icons CSS -->
    <link href="css/grayscale.css" rel="stylesheet"><!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://use.typekit.net/bcs5lch.js"></script>
    <script>
    try{Typekit.load({ async: true });}catch(e){}
    </script>
</head>
<body data-spy="scroll" data-target=".navbar-fixed-top" id="page-top">

<!-- Google Tag Manager -->
<noscript>iframe src="//www.googletagmanager.com/ns.html?id=GTM=W9VL5P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W9VL5P');</script>
<!-- End Google Tag Manager -->

    <!-- Intro Header -->
    <header class="intro">
      <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2" style="z-index: 100000;">
                        <img alt="Entralife Logo" class="img-responsive brand-heading center-block" src="http://mcclainfoundation.org/img/logo.png">
                        <p class="intro-text">Making Dreams a Reality</p><a class="btn btn-circle page-scroll" href="#about"><i class="fa fa-angle-double-down animated"></i></a>
                    </div>
                </div>                
            </div>
            <div id="wistia_xb1p8tfldf" class="wistia_embed backgroundVideo" style="width:100%;height:100%;z-index: -1;"></div>
 <div id="video-viewport">
            <!-- <video autoplay controls preload width="640" height="360">
        <source src="http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4"type="video/mp4" />
        <source src="http://www.quirksmode.org/html5/videos/big_buck_bunny.webm"type="video/webm" />
        <source src="http://www.quirksmode.org/html5/videos/big_buck_bunny.ogv"type="video/webm" />
    </video> -->
    	  <!--<div id="wistia_srl9z74bwz" class="wistia_embed backgroundVideo" style="width:100%;height:100vh;"></div>        -->
    
          <!--<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
          <div class="wistia_embed wistia_async_srl9z74bwz videoFoam=true" style="height:100%;width:100%">&nbsp;</div>
            <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
                <div class="wistia_embed wistia_async_srl9z74bwz videoFoam=true" style="height:100%;width:100%">&nbsp;</div>
            </div>-->
        </div>
        
        
        </div>
        </div>
        
        <!-- Video -->
        <div id="debug1"></div>
    </header><!-- About Section -->
    <section class="about-text" id="about">
        <div class="container content-section text-center">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2>WHAT IS The MCCLAIN FOUNDATION?</h2>
                    <p>McClain Foundation was founded to bring hope to men, women, families, children and groups that are seeking a helping hand to achieve their dreams.  The organization has many philanthropic endeavors that are meant to be a bridge between a great idea and a desperate need.  We seek to help anyone that has a just cause, a strong heart, a faithful spirit, and desires to rise above the common level to grow into who they were meant to be.</p>
                    <a class="btn btn-default btn-lg text-uppercase" data-target="#Modal01" data-toggle="modal">Request More Information</a>
                </div>
            </div>
        </div>
    </section><!-- Modal 01 -->
    <div aria-labelledby="myModalLabel" class="modal fade" id="Modal01" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                    <div class="row">
                        <div class=" col-sm-10 col-sm-offset-1">
                            <h4 class="modal-title text-uppercase" id="Modal01Label">Apply for a membership</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                	<form role="form" name="applyForm" id="applyForm" action="apply.php" method="POST">
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <input class="form-control input-lg" name="fname" id="fname" placeholder="FIRST NAME" type="text">
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <input class="form-control input-lg" name="lname" id="lname" placeholder="LAST NAME" type="text">
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <input class="form-control input-lg" name="email" id="email" placeholder="E-MAIL" type="text">
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <input class="form-control input-lg" name="password" id="password" placeholder="PASSWORD" type="password">
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <input class="form-control input-lg" name="passwordc" id="passwordc" placeholder="CONFIRM PASSWORD" type="password">
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <select class="c-select form-control input-lg crs-country" name="country" id="country" data-region-id="one" data-value="shortcode"></select>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <select class="c-select form-control input-lg" name="state" id="one"></select>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1 text-center">
	                            <input type="submit" aria-label="Submit" class="btn btn-default btn-lg" value="SUBMIT">
	                        </div>
	                    </div>
                    </form>
                </div>

                <div style="visibility:hidden;display:none;">
                    <button type="button" class="modal01-close" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </div><!-- Modal 02 -->
    <div aria-labelledby="myModalLabel" class="modal fade" id="Modal02" role="dialog" tabindex="-1">
    <a style="display:none;" class="modal02" data-target="#Modal02" data-toggle="modal" aria-hidden="true"></a>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                    <div class="row">
                        <div class=" col-sm-10 col-sm-offset-1">
                            <h4 class="modal-title" id="Modal02Label">IMPORTANT</h4>
                            <p>GET ON OUR PRIVATE VIP<br>
                            INSIDER’S NOTIFICATION LIST</p>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <form role="form" name="applyFormVip" id="applyFormVip" action="applyVip.php" method="POST">
                        <input type="hidden" name="userid" id="userid" value="" aria-hidden="true" />
                        <div class="row">
                            <div class=" col-sm-10 col-sm-offset-1 modal-sub">
                                <h4>AS A VIP, YOU GET:</h4>
                                <p>Important information you need instantly on your phone, SMS reminder 30 minutes before live webinar starts, Private VIP INVITE to "advanced Cutting Edge Traffic" Online Live Class, Unannounced surprize bonuses and gifts, And Much More!</p>
                                <p>Simply select your country and enter your mobile numberin the form below you will instantly be added to our vipinsider’s notification list and recieve a confirmation SMS</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-sm-10 col-sm-offset-1">
                                <select class="c-select form-control input-lg crs-country" name="country2" id="country2" data-region-id="two"></select>
                                <select style="display:none;visibility:hidden;" id="two"></select>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-sm-10 col-sm-offset-1">
                                <input class="form-control input-lg" placeholder="MOBILE NUMBER" type="number" name="mobile" id="mobile">
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-sm-10 col-sm-offset-1 text-center">

                                <input type="submit" aria-label="Submit" class="btn btn-default btn-lg" value="SUBMIT">
                            </div>
                        </div>

                        <div style="visibility:hidden;display:none;">
                            <button type="button" class="modal02-close" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- Modal 03 -->
    <div aria-labelledby="myModalLabel" class="modal fade" id="Modal03" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                    <div class="row">
                        <div class=" col-sm-10 col-sm-offset-1">
                            <h4 class="modal-title" id="Modal03Label">THANK YOU</h4>
                            <p>FOR REQUESTING ACCESS TO ENTRALIFE</p>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class=" col-sm-10 col-sm-offset-1 modal-sub">
                            <p>Your application is received and is currently pending. If and when your application is approved you will be notified via email.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-sm-10 col-sm-offset-1 text-center">
                            <a aria-label="Close" class="btn btn-default btn-lg modal03-close" data-dismiss="modal" data-toggle="modal">CLOSE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Footer -->
    <div style="visibility:hidden;display:none;">
        <a class="modal01" data-target="#Modal01" data-toggle="modal">Apply for a membership</a>
        
        <a class="modal03" data-target="#Modal03" data-toggle="modal"></a>
    </div>
    <input type="hidden" name="complete1" id="complete1" value="" />
    <footer>
        <div class="container text-center">
            <div class="row">
                <div class="text-center foot-img"><img alt="Entralife Logo" class="img-responsive center-block" height="22.643" src="http://mcclainfoundation.org/img/logo_w.png" width="181.594"></div>
            </div>
            <p>Terms &amp; Privacy Policy DMCA Notice Code of Conduct<br>
            &copy; 2016 - McClainfoundation.org - All Rights Reserved.</p><br>
            <p>McClain Foundation, LLC 30161 Avenida de las Banderas, Suite C, Rancho Santa Margarita, CA 92688</p>
            <div class="social-wrap">
            	<a href="https://www.facebook.com/mcclainfoundation/" class="btn btn-social-icon btn-facebook" target="new"><span class="fa fa-facebook"></span></a>
            	<a href="https://twitter.com/McClainFDN" class="btn btn-social-icon btn-twitter" target="new"><span class="fa fa-twitter"></span></a>
            	<a href="https://plus.google.com/114024923073706205893/" class="btn btn-social-icon btn-google" target="new"><span class="fa fa-google"></span></a>
                <a href="https://www.youtube.com/channel/UCarTwVB20_5p_nkT5z0dkww" class="btn btn-social-icon btn-youtube" target="new"><span class="fa fa-youtube"></span></a>
                <a href="https://www.pinterest.com/mcclainfdn/" class="btn btn-social-icon btn-pinterest" target="new"><span class="fa fa-pinterest"></span></a>
                <a href="https://www.instagram.com/mcclainfdn/?hl=en" class="btn btn-social-icon btn-instagram" target="new"><span class="fa fa-instagram"></span></a>
            </div>
        </div>
    </footer><!-- jQuery -->

    <script src="js/jquery.js"></script> 

    <script src="js/jquery.validate.min.js"></script> 

    <script src="js/country-region-selector/dist/crs.js"></script> 

    <script src="js/bootstrap-select.js"></script> <!-- Bootstrap Core JavaScript -->
     
    <script src="js/bootstrap.min.js"></script> <!-- Plugin JavaScript -->
     
    <script src="js/jquery.easing.min.js"></script>
    
    

    <script src="js/main.js"></script>

    <?php /*
     <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
     <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script> -->
     <!-- Custom Theme JavaScript --> */ ?>
    <script charset='ISO-8859-1' src='https://fast.wistia.com/assets/external/E-v1.js'></script>
	<script charset='ISO-8859-1' src='https://fast.wistia.com/labs/crop-fill/plugin.js'></script> 
    <script src="js/grayscale.js"></script>
	
    <style>
        label.error {color:red;}
    </style>
</body>
</html>
