<!DOCTYPE html>
<html lang="en">
<head>

</head>
<body data-spy="scroll" data-target=".navbar-fixed-top" id="page-top">


                <div class="modal-body">
                	<form role="form" name="applyForm" id="applyForm" action="apply-test.php" method="POST">
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <input class="form-control input-lg" name="fname" id="fname" placeholder="FIRST NAME" type="text">
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <input class="form-control input-lg" name="lname" id="lname" placeholder="LAST NAME" type="text">
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <input class="form-control input-lg" name="email" id="email" placeholder="E-MAIL" type="text">
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <input class="form-control input-lg" name="password" id="password" placeholder="PASSWORD" type="password">
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <input class="form-control input-lg" name="passwordc" id="passwordc" placeholder="CONFIRM PASSWORD" type="password">
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <select class="c-select form-control input-lg crs-country" name="country" id="country" data-region-id="one" data-value="shortcode"></select>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1">
	                            <select class="c-select form-control input-lg" name="state" id="one"></select>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class=" col-sm-10 col-sm-offset-1 text-center">
	                            <input type="submit" aria-label="Submit" class="btn btn-default btn-lg" value="SUBMIT">
	                        </div>
	                    </div>
                    </form>
                </div>

</body>
</html>
